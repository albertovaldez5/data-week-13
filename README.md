# Leaflet

Leaflet is one of the many mapping APIs that exist. It is lightweight and performant and has an extensive documentation we can use for reference. <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>


## Including Leaflet

We can include Leaflet in our HTML file by including both the `CSS` and the `JS` in our header. As long as it is before scripts that require it.

```html
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
```

Now we need a container where the map will load.

```html
<div id="map"></div>
```

Then in our `body` we will load our API key script and our mapping script.

```html
<script type="text/javascript" src="../resources/01/config.js"></script>
<script type="text/javascript" src="../resources/01/logic.js"></script>
```


## Implementing Leaflet

We are going to use tiles from Mapbox, which is a tile service provider <sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>. They split their assets into many pieces and deliver them to our browser whenever we do a request. The reason to do that instead of downloading a single larger image is to improve speed of delivery of the data. We can adjust the size of the individual tile pieces by changing the `tileSize` property, lower values mean faster loading times but lower quality.

```typescript
// Create a map object
var myMap = L.map("map", {
    center: [37.09, -95.71],
    zoom: 5
  });

// Add a tile layer
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);
```

<div class="org" id="org7ccf826">

<div id="orga3e9263" class="figure">
<p><img src="../resources/01.png" alt="01.png" width="700" />
</p>
</div>

</div>

The `div` elements used to display the map don&rsquo;t have a height by default so we must specify it in order to display a map properly. We can do that process automatically by incorporating our own css stylesheet.

This CSS sets the map, body, and html to 100% of the screen size.

```css
#map,
body,
html {
  height: 100%;
}
```


# Drawing Markers

We can add markers to our map using `L.marker` <sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup>.

```typescript
var marker = L.marker([40.7128, 74.0060])
marker.addTo(map);
```


## Custom icons

We can add a custom icon to our marker by loading a png image.

```typescript
// Create a map object
var myMap = L.map("map", {
    center: [37.09, -95.71],
    zoom: 5
});
// Add a tile layer
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);
var myIcon = L.icon({
    iconUrl: '../restaurant.png',
    iconSize: [50, 50]
});
var marker = L.marker([37.09, -95.71]{
    icon: myIcon,
    draggable: true,
    title: "My Marker"
}).addTo(myMap);
```

<div class="org" id="orgc81de0f">

<div id="org8fcf138" class="figure">
<p><img src="../resources/03b.png" alt="03b.png" width="700" />
</p>
</div>

</div>


## Example: Multiple Markers

This is the data for all our markers. We want to add a marker per city.

```typescript
var cities = [{
  location: [40.7128, -74.0059],
  name: "New York",
  population: 8550405
},
{
  location: [41.8781, -87.6298],
  name: "Chicago",
  population: 2720546
},
{
  location: [29.7604, -95.3698],
  name: "Houston",
  population: 2296224
},
{
  location: [34.0522, -118.2437],
  name: "Los Angeles",
  population: 3971883
},
{
  location: [41.2524, -95.9980],
  name: "Omaha",
  population: 446599
}
];
```

We will create a the `myMap` object and then add all the markers using `forEach` in the cities array.

```typescript
// Create a map object
var myMap = L.map("map", {
    center: [37.09, -95.71],
    zoom: 4
});
// Add a tile layer
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);

cities.forEach(function(city){
    let markerLayer = L.marker(city.location);
    markerLayer.addTo(myMap);
    markerLayer.bindPopup(city.name);
});
```

**NOTE**: We are using `bindPopup` to add more information to the marker when it is clicked. <sup><a id="fnr.4" class="footref" href="#fn.4" role="doc-backlink">4</a></sup>

Assumming our script name is `markers.js` and the data is named `cities.js`, we must make sure our HTML body looks like this.

```html
<script type="text/javascript" src="config.js"></script>
<script type="text/javascript" src="cities.js"></script>
<script type="text/javascript" src="markers.js"></script>
```

<div class="org" id="orgd790fb8">

<div id="orge0fc38a" class="figure">
<p><img src="../resources/03.png" alt="03.png" width="700" />
</p>
</div>

</div>


# Drawing Shapes

We can draw shapes like circles or rectangles. In this case we&rsquo;ve added the following code to the previous code snippet.

```typescript

cities.forEach(function(city){
    console.log(city.population/40);
    let circle = L.circle(city.location, {
        radius: city.population / 40,
        fillOpacity: 0.75,
        color: "white",
        fillColor: "purple"
    });
    circle.addTo(myMap);
});
```

<div class="org" id="org2467531">

<div id="org3622b64" class="figure">
<p><img src="../resources/03c.png" alt="03c.png" width="700" />
</p>
</div>

</div>


## Example: Drawing

Let&rsquo;s say we have the following data named `countries.js`.

```typescript
// Country data
var countries = [
    {
      name: "Brazil",
      location: [-14.2350, -51.9253],
      points: 237
    },
    {
      name: "Germany",
      location: [51.1657, 10.4515],
      points: 221
    },
    {
      name: "Italy",
      location: [41.8719, 12.5675],
      points: 156
    },
    {
      name: "Argentina",
      location: [-38.4161, -63.6167],
      points: 144
    },
    {
      name: "France",
      location: [46.2276, 2.2137],
      points: 115
    },
    {
      name: "England",
      location: [52.355, 1.1743],
      points: 108
    },
    {
      name: "Spain",
      location: [40.4637, -3.7492],
      points: 105
    },
    {
      name: "Netherlands",
      location: [52.1326, 5.2913],
      points: 93
    },
    {
      name: "Uruguay",
      location: [-32.4228, -55.7658],
      points: 84
    },
    {
      name: "Sweden",
      location: [60.1282, 18.6435],
      points: 70
    }
  ];
```

We must make sure that the data is loaded in the HTML.

```html
<script type="text/javascript" src="cities.js"></script>
<script type="text/javascript" src="logic.js"></script>
```

We can load the data with a `forEach` and display them in the map. We want the circles to have a different color depending on the `points` property of each data point.

```typescript
// Create a map object
var myMap = L.map("map", {
  center: [15.5994, -28.6731],
  zoom: 3
});

// Adding tile layer
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
  attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
  tileSize: 512,
  maxZoom: 18,
  zoomOffset: -1,
  id: "mapbox/streets-v11",
  accessToken: API_KEY
}).addTo(myMap);

// Function to determine color
function circleColor(points){
    if (points >= 200){
        return 'blue';
    }
    else if (points > 150 && points < 200){
        return 'green';
    }
    else if (points > 100 && points < 150){
       return'yellow';
    }
    return 'red';
}

// Loop through the countries array
countries.forEach(function(country){

    let circleLayer = L.circle(country.location, {
        'radius': country.points * 1000,
        'fillOpacity': 0.75,
        'fillColor': circleColor(country.points),
        'color': "white"
    })
    circleLayer.addTo(myMap);
    circleLayer.bindPopup(`<h1>${country.name}</h1><hr><h3>${country.points}`);
});
```

<div class="org" id="org8663e16">

<div id="orga5a40fb" class="figure">
<p><img src="../resources/05.png" alt="05.png" width="700" />
</p>
</div>

</div>


# Layer Controls

Layer controls help us display different layers depending on the options we select <sup><a id="fnr.5" class="footref" href="#fn.5" role="doc-backlink">5</a></sup>. There will be a menu in the top right of the page that allows us to change between layers.

Normally, all markers would exist in a different layer each, in order to have them in a single layer we can create a `layerGroup` from an array of markers. Then we will add it to our `overlayMaps` object to use later.

```typescript
var littleton = L.marker([39.61, -105.02]).bindPopup('This is Littleton, CO.'),
    denver    = L.marker([39.74, -104.99]).bindPopup('This is Denver, CO.'),
    aurora    = L.marker([39.73, -104.8]).bindPopup('This is Aurora, CO.'),
    golden    = L.marker([39.77, -105.23]).bindPopup('This is Golden, CO.');
var cities = L.layerGroup([littleton, denver, aurora, golden]);
var overlayMaps = {
    Cities: cities
};
```

Control layers require a base layer and an overlay layer. Our overlay layer is already `overlayMaps` and our base layer will be `baseMaps` containing `light` and `dark`.

```typescript
var light = L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
});
var dark = L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/dark-v10",
    accessToken: API_KEY
});
var baseMaps = {
    Light: light,
    Dark: dark
};
```

Now that we have `baseMaps` and `overlayMaps` we can create our layer with them and then add it to the map.

```typescript


var myMap = L.map('map', {
    center: [39.73, -104.99],
    zoom: 10,
    layers: [dark, cities]
});
var layerControl = L.control.layers(baseMaps, overlayMaps).addTo(myMap);
```

<div class="org" id="org6f04e1c">

<div id="org75b008b" class="figure">
<p><img src="../resources/05b.png" alt="05b.png" width="700" />
</p>
</div>

</div>

Note that we could add more layer options dynamically.

```typescript
var satellite = L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/satellite-v9",
    accessToken: API_KEY
});
layerControl.addBaseLayer(satellite, "Satellite");
```


## Mapbox Styles

We can use the Mapbox API <sup><a id="fnr.6" class="footref" href="#fn.6" role="doc-backlink">6</a></sup> to get different styles and use them in our map. We use them in our `tileLayer` objects by copying the `style URL`. For example:

| Map ID              |
|------------------- |
| mapbox/streets-v11  |
| mapbox/outdoors-v11 |
| mapbox/light-v10    |
| mapbox/dark-v10     |
| mapbox/satellite-v9 |


# JSON and GeoJSON

JSON is a standard in which the community has agreed to exchange information<sup><a id="fnr.7" class="footref" href="#fn.7" role="doc-backlink">7</a></sup>.

GeoJSON is built on top of JSON and it is the way the mapping community has agreed to distribute the data. It can have three levels of data categories.<sup><a id="fnr.8" class="footref" href="#fn.8" role="doc-backlink">8</a></sup> We can use `geojson.io`<sup><a id="fnr.9" class="footref" href="#fn.9" role="doc-backlink">9</a></sup> to see examples of GeoJSON data and create our own.

**FeatureCollection**: An array of features, which includes geometry and properties.

1.  **type** the type of the feature.
2.  **geometry** can include geometry information in the way of points, polygons or polylines. It also includes **coordinates** which can be either [lat, long] or [long, lat].
3.  **properties** includes other information of the mapping data.

```json
{
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [125.6, 10.1]
  },
  "properties": {
    "name": "Dinagat Islands"
  }
}
```

Leaflet has a function to automatically handle GeoJSON data and create all the data for the map<sup><a id="fnr.10" class="footref" href="#fn.10" role="doc-backlink">10</a></sup>.

```typescript
L.geoJSON(data).addTo(myMap);
```

We can give leaflet more arguments through the `options` object, denoted by the `{}` syntax.

```typescript
L.geoJson(data, {
    onEachFeature: function(feature, layer){
        layer.bindPopup(
            `<h3>${feature.properties.place}</h3><hr><h3>${feature.properties.time}</h3>`
        );
    }
}).addTo(myMap);
```


# GeoJSON Example


## GeoJSON file

The data will be at `static/data/nyc.geojson`. It contains polygons as the geometry type. In `features` we have the data from the locations, in this case our locations are NYC neighborhoods. The polygons will draw the perimeter of each neighborhood.

```json
{
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "properties": {
                "neighborhood": "Allerton",
                "boroughCode": "2",
                "borough": "Bronx",
                "@id": "http:\/\/nyc.pediacities.com\/Resource\/Neighborhood\/Allerton"
        },
            "geometry": {
                "type": "Polygon",
                "coordinates": [ [ [ -73.848597000000183, 40.871670000000115 ],
```

Our HTML will make sure to load our logic script, named `logic4.js` and before that the `config.js` with the `API_KEY` constant.

```html
<script type="text/javascript" src="./static/js/config.js"></script>
<script type="text/javascript" src="static/js/logic4.js"></script>
```

And in the Javascript code we will need to follow these step:

1.  Create a map object
2.  Add a `tileLayer`.
3.  Create a variable to store the path to the GeoJson data.
4.  Create an utility function named `chooseColor` that we will use to pick colors depending on a feature value.
5.  Use `d3.json` to load our data as a `promise`.
6.  Use `L.geoJSON` to create the polygons and customize them when the promsie is `fullfilled`.


## Basic Map Code

We start with the first part of the boilerplate code by creating our map.

```typescript
var myMap = L.map("map", {
  center: [40.7, -74],
  zoom: 11,
});
```

Second part of the boilerplate code where we create the tile Layer and add it to the map.

```typescript
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);
```

This is the location of our geojson data. It can be an URL too.

```typescript
var link = "static/data/nyc.geojson";
```

Then we will create the utility function to help us pick a color depending on the borough property.

```typescript
function chooseColor(borough) {
  switch(borough){
    case "Brooklyn": return "yellow"
    case "Bronx": return "red"
    case "Manhattan": return "orange"
    case "Queens": return "green"
    case "Staten Island": return "purple"
    default: return "black"
  }
}
```

Now we make our request with D3. Once the promise is fullfilled, `then` is executed, which will call the Leaflet function to create all the mapping data. However we will look at the parts of `L.geoJSON` one by one.


## Using GeoJSON data

```typescript
d3.json(link).then((data) => {
  L.geoJSON(data, {
```

We pass `data` first, then an object containing options<sup><a id="fnr.11" class="footref" href="#fn.11" role="doc-backlink">11</a></sup>. The first option is `style`, which will use an anonymus function that returns a object with even more options. In this case we are using `fillColor` with our custom utility function, then `stroke` and `fillOpacity`.

```typescript
    style: (feature) => {
      return {
        fillColor: chooseColor(feature.properties.borough),
        stroke: "white",
        fillOpacity: 0.5
      };
    },
```


## On Each Feature

Next we will use the `onEachFeature` option. Which uses an anonymus function that receives the `feature` and the `layer`. Inside this function we will call the `layer.on` function, which takes an object as argument. That object will receive `mouseover` and `mouseout` parameters<sup><a id="fnr.12" class="footref" href="#fn.12" role="doc-backlink">12</a></sup> which are basically event listeners. So when whatever they are listening to occurs, the function they have as parameter is called. In this case we are setting the `layer` of `onEachFeature` to the target of the even (whatever triggered it) and then we are calling the method `setStyle` from the layer.

```typescript
    onEachFeature: (feature, layer) => {
      layer.on({
        mouseover: (event) => {
          layer = event.target;
          layer.setStyle({fillOpacity: 1});
        },
        mouseout: (event) => {
          layer=event.target;
          layer.setStyle({fillOpacity: 0.5});
        }
      });
```

We are still inside `onEachFeature`, and we want to add one last thing, a `popup` to each feature depending on their properties. In this case we add two HTML elements with relevant data from the feature. Then we close the rest of the parentheses and add

```typescript
      layer.bindPopup(
        `<h3>${feature.properties.neighborhood}</h3>
        <h4>${feature.properties.borough}</h4>
        `);
    },
  }).addTo(myMap); // close L.geoJSON(data, {})
}); // close .then(function(data){})
```


## Results

The final script looks like this.

```typescript
var myMap = L.map("map", {
  center: [40.7, -74],
  zoom: 11,
});
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);
var link = "static/data/nyc.geojson";
function chooseColor(borough) {
  switch(borough){
    case "Brooklyn": return "yellow"
    case "Bronx": return "red"
    case "Manhattan": return "orange"
    case "Queens": return "green"
    case "Staten Island": return "purple"
    default: return "black"
  }
}
d3.json(link).then((data) => {
  L.geoJSON(data, {
    style: (feature) => {
      return {
        fillColor: chooseColor(feature.properties.borough),
        stroke: "white",
        fillOpacity: 0.5
      };
    },
    onEachFeature: (feature, layer) => {
      layer.on({
        mouseover: (event) => {
          layer = event.target;
          layer.setStyle({fillOpacity: 1});
        },
        mouseout: (event) => {
          layer=event.target;
          layer.setStyle({fillOpacity: 0.5});
        }
      });
      layer.bindPopup(
        `<h3>${feature.properties.neighborhood}</h3>
        <h4>${feature.properties.borough}</h4>
        `);
    },
  }).addTo(myMap); // close L.geoJSON(data, {})
}); // close .then(function(data){})
```

<div class="org" id="org9553a15">

<div id="orgc8c77e5" class="figure">
<p><img src="../resources/03x.png" alt="03x.png" width="700" />
</p>
</div>

</div>

Whenever we hove each borough, it should change opacity, and whenever we click on it, it should display the popup with the information.


# Leaflet Plugins

TODO: Add information about Leaflet plugins.

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://leafletjs.com/reference.html>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://docs.mapbox.com/api/overview/>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> <https://leafletjs.com/reference.html#marker>

<sup><a id="fn.4" class="footnum" href="#fnr.4">4</a></sup> <https://leafletjs.com/reference.html#popup>

<sup><a id="fn.5" class="footnum" href="#fnr.5">5</a></sup> <https://leafletjs.com/examples/layers-control/>

<sup><a id="fn.6" class="footnum" href="#fnr.6">6</a></sup> <https://docs.mapbox.com/api/maps/styles/>

<sup><a id="fn.7" class="footnum" href="#fnr.7">7</a></sup> <https://www.json.org/json-en.html>

<sup><a id="fn.8" class="footnum" href="#fnr.8">8</a></sup> <https://geojson.org/>

<sup><a id="fn.9" class="footnum" href="#fnr.9">9</a></sup> <https://geojson.io/>

<sup><a id="fn.10" class="footnum" href="#fnr.10">10</a></sup> <https://leafletjs.com/examples/geojson/>

<sup><a id="fn.11" class="footnum" href="#fnr.11">11</a></sup> <https://leafletjs.com/reference.html#geojson-option>

<sup><a id="fn.12" class="footnum" href="#fnr.12">12</a></sup> <https://leafletjs.com/reference.html#interactive-layer>
