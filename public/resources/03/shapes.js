// Create a map object
var myMap = L.map("map", {
    center: [37.09, -95.71],
    zoom: 4
});
// Add a tile layer
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);
cities.forEach(function (city) {
    var markerLayer = L.marker(city.location);
    markerLayer.addTo(myMap);
    markerLayer.bindPopup(city.name);
});
cities.forEach(function (city) {
    console.log(city.population / 40);
    var circle = L.circle(city.location, {
        radius: city.population / 40,
        fillOpacity: 0.75,
        color: "white",
        fillColor: "purple"
    });
    circle.addTo(myMap);
});
