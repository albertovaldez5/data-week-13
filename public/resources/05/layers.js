var littleton = L.marker([39.61, -105.02]).bindPopup('This is Littleton, CO.'), denver = L.marker([39.74, -104.99]).bindPopup('This is Denver, CO.'), aurora = L.marker([39.73, -104.8]).bindPopup('This is Aurora, CO.'), golden = L.marker([39.77, -105.23]).bindPopup('This is Golden, CO.');
var cities = L.layerGroup([littleton, denver, aurora, golden]);
var overlayMaps = {
    Cities: cities
};
var light = L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
});
var dark = L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/dark-v10",
    accessToken: API_KEY
});
var baseMaps = {
    Light: light,
    Dark: dark
};
var myMap = L.map('map', {
    center: [39.73, -104.99],
    zoom: 10,
    layers: [dark, cities]
});
var layerControl = L.control.layers(baseMaps, overlayMaps).addTo(myMap);
