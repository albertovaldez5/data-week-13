// Create a map object
var myMap = L.map("map", {
    center: [15.5994, -28.6731],
    zoom: 3
});
// Adding tile layer
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);
// Function to determine color
function circleColor(points) {
    if (points >= 200) {
        return 'blue';
    }
    else if (points > 150 && points < 200) {
        return 'green';
    }
    else if (points > 100 && points < 150) {
        return 'yellow';
    }
    return 'red';
}
// Loop through the countries array
countries.forEach(function (country) {
    var circleLayer = L.circle(country.location, {
        'radius': country.points * 1000,
        'fillOpacity': 0.75,
        'fillColor': circleColor(country.points),
        'color': "white"
    });
    circleLayer.addTo(myMap);
    circleLayer.bindPopup("<h1>".concat(country.name, "</h1><hr><h3>").concat(country.points));
});
