// Creating map object
var myMap = L.map("map", {
  center: [40.7, -74],
  zoom: 11,
});
  
// Add a tile layer
L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
    attribution: "© <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>",
    tileSize: 512,
    maxZoom: 18,
    zoomOffset: -1,
    id: "mapbox/streets-v11",
    accessToken: API_KEY
}).addTo(myMap);

var link = "static/data/nyc.geojson";
// Create a function that will determine the color of a neighborhood based on the borough it belongs to
function chooseColor(borough) {
  switch(borough){
    case "Brooklyn": return "yellow"
    case "Bronx": return "red"
    case "Manhattan": return "orange"
    case "Queens": return "green"
    case "Staten Island": return "purple"
    default: return "black"
  }
}
// Get our GeoJSON data using d3.json
// Create a geoJSON layer with the retrieved data
// for the fillColor use the chooseColor function and pass in the borough object from the GeoJSON data
// Call on each feature
// Set mouse events to change map styling
// When a user's mouse touches a map feature, the mouseover event calls this function, that feature's opacity changes to 90% so that it stands out
// When the cursor no longer hovers over a map feature - when the mouseout event occurs - the feature's opacity reverts back to 50%
// When a feature (neighborhood) is clicked, it is enlarged to fit the screen
// Giving each feature a pop-up with information 
d3.json(link).then((data) => {
  L.geoJSON(data, {
    style: (feature) => {
      return {
        fillColor: chooseColor(feature.properties.borough),
        stroke: "white",
        fillOpacity: 0.5
      };
    },
    onEachFeature: (feature, layer) => {
      layer.on({
        mouseover: (event) => {
          layer = event.target;
          layer.setStyle({fillOpacity: 1});
        },
        mouseout: (event) => {
          layer=event.target;
          layer.setStyle({fillOpacity: 0.5});
        }
      });
      layer.bindPopup(
        `<h3>${feature.properties.neighborhood}</h3>
        <h4>${feature.properties.borough}</h4>
        `);
    },
  }).addTo(myMap);
});
